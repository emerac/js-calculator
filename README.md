# js-calculator

A webpage that displays a fully-functioning calculator.

The motivation behind making this webpage was to practice JavaScript.

## Usage

[Click here](https://emerac.gitlab.io/js-calculator) to visit the live page!

Alternatively, you can clone this repository and then open the `index.html`
file in your browser.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.
