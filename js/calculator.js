/* Perform basic calculations and manage a calculator display.
 *
 * Copyright (C) 2021 emerac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
function add(x, y) {
    return x + y;
}


function addPushedState(operator) {
    const operatorNodes = document.querySelectorAll('.calc-operator-button');
    const operatorButtons = Array.from(operatorNodes);
    const operatorButton = operatorButtons.filter(button => (
        (button.textContent === operator) ? true : false)
    )[0];
    operatorButton.classList.add('calc-operator-button-active');
}


function changeSign() {
    const display = document.querySelector('.calc-display');
    if (isDisplayEmpty()) {
        return;
    }

    currentSideOfOperator = getCurrentSideOfOperator();
    currentOperand = currentEquation[currentSideOfOperator];
    if (currentOperand.startsWith('-')) {
        newOperand = currentOperand.slice(1,);
    } else {
        newOperand = '-' + currentOperand;
    }
    currentEquation[currentSideOfOperator] = newOperand;
    updateDisplay(currentEquation[currentSideOfOperator]);
}


function clearCurrentEquation() {
    for (const prop in currentEquation) {
        currentEquation[prop] = '';
    }
}


function clearDisplay() {
    const display = document.querySelector('.calc-display');
    clearCurrentEquation();
    currentSideOfOperator = getCurrentSideOfOperator();
    currentOperand = currentEquation[currentSideOfOperator];
    updateDisplay(currentOperand);
    removePushedStates();
}


function convertToPercentage() {
    const display = document.querySelector('.calc-display');
    if (isDisplayEmpty()) {
        return;
    }

    currentSideOfOperator = getCurrentSideOfOperator();
    currentOperand = currentEquation[currentSideOfOperator];
    const newOperand = String(Number(currentOperand) / 100);
    currentEquation[currentSideOfOperator] = newOperand;
    if (newOperand.length < MAX_DISPLAY_LENGTH) {
        updateDisplay(currentEquation[currentSideOfOperator]);
    } else {
        operandToDisplay = Number(currentEquation[currentSideOfOperator]);
        updateDisplay(String(operandToDisplay.toFixed(MAX_DISPLAY_LENGTH)));
    }
}


function createInterface() {
    const buttonLabels = [
        ['AC', '↩', '±', '÷'],
        ['7', '8', '9', '×'],
        ['4', '5', '6', '−'],
        ['1', '2', '3', '+'],
        ['0', '.', '%', '='],
    ];
    const buttonsContainer = document.querySelector('.calc-buttons-container');

    for (let i = 0; i < buttonLabels.length; i++) {
        for (let j = 0; j < buttonLabels[0].length; j++) {
            const button = document.createElement('button');
            button.classList.add('calc-button');
            button.textContent = buttonLabels[i][j];
            buttonsContainer.appendChild(button);


            switch (button.textContent) {
                case 'AC':
                    button.classList.add('calc-function-button');
                    button.addEventListener('click', clearDisplay);
                    break;
                case '↩':
                    button.classList.add('calc-function-button');
                    button.addEventListener('click', undoDisplay);
                    break;
                case '±':
                    button.classList.add('calc-function-button');
                    button.addEventListener('click', changeSign);
                    break;
                case '÷':
                case '×':
                case '−':
                case '+':
                    button.classList.add('calc-operator-button');
                    button.addEventListener(
                        'click',
                        e => insertOperator(e.target.textContent)
                    );
                    break;
                case '=':
                    button.classList.add('calc-operator-button');
                    button.addEventListener('click', performCalculation);
                    break;
                case '.':
                    button.classList.add('calc-regular-button');
                    button.addEventListener('click', displayDecimal);
                    break;
                case '%':
                    button.classList.add('calc-regular-button');
                    button.addEventListener('click', convertToPercentage);
                    break;
                default:
                    button.classList.add('calc-regular-button');
                    button.addEventListener(
                        'click',
                        e => displayNumber(e.target.textContent),
                    );
            }
        }
    }

    document.addEventListener('keydown', e => {
        switch (e.key) {
            case 'c':
            case 'C':
                clearDisplay();
                break;
            case 'Backspace':
                undoDisplay();
                break;
            case '/':
                insertOperator('÷');
                break;
            case '*':
                insertOperator('×');
                break;
            case '-':
                insertOperator('−');
                break;
            case '+':
                insertOperator('+');
                break;
            case '=':
            case 'Enter':
                performCalculation();
                break;
            case '.':
                displayDecimal();
                break;
            case '%':
                convertToPercentage();
                break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                displayNumber(e.key);
                break;
            default:
                // All other keys are irrelevant, but shouldn't cause an error.
                ;
        }
    });
}


function displayDecimal() {
    const display = document.querySelector('.calc-display');
    if (isDisplayEmpty()) {
        return;
    }

    currentSideOfOperator = getCurrentSideOfOperator();
    currentOperand = currentEquation[currentSideOfOperator];
    if (!(currentOperand.includes('.'))) {
        newOperand = currentOperand + '.';
        currentEquation[currentSideOfOperator] = newOperand;
        updateDisplay(currentEquation[currentSideOfOperator]);
    }
}


function displayNumber(number) {
    currentSideOfOperator = getCurrentSideOfOperator();
    currentOperand = currentEquation[currentSideOfOperator];

    // Scientific notation numbers should not be 'editable'.
    if (currentOperand.includes('e')) {
        return;
    }

    newOperand = currentOperand + number;
    currentEquation[currentSideOfOperator] = newOperand;
    updateDisplay(currentEquation[currentSideOfOperator]);
}


function divide(x, y) {
    if (y === 0) {
        return 'error :(';
    } else {
        return x / y;
    }
}


function getCurrentSideOfOperator() {
    if (currentEquation['operator'] === '') {
        return 'leftOperand';
    } else {
        return 'rightOperand';
    }
}


function insertOperator(operator) {
    if (currentEquation['leftOperand'] === '') {
        return;
    }

    if (!(currentEquation['operator'] === '')) {
        performCalculation();
    }

    // Only insert new operator if `performCalculation` didn't fail.
    if (currentEquation['operator'] === '') {
        currentEquation['operator'] = operator;
        addPushedState(operator);
    }
}


function isDisplayEmpty() {
    const display = document.querySelector('.calc-display');
    if (display.textContent === '') {
        return true;
    } else {
        return false;
    }
}


function isValidEquation() {
    for (const prop in currentEquation) {
        if (currentEquation[prop] === '') {
            return false;
        }
    }
    return true;
}


function multiply(x, y) {
    return x * y;
}


function operate(leftOperand, operator, rightOperand) {
    switch (operator) {
        case '+':
            return add(leftOperand, rightOperand);
        case '÷':
            return divide(leftOperand, rightOperand);
        case '×':
            return multiply(leftOperand, rightOperand);
        case '−':
            return subtract(leftOperand, rightOperand);
        default:
            throw new Error('Unreachable');
    }
}


function performCalculation() {
    if (!isValidEquation()) {
        return;
    }

    const leftOperand = Number(currentEquation['leftOperand']);
    const operator = currentEquation['operator'];
    const rightOperand = Number(currentEquation['rightOperand']);
    const result = String(operate(leftOperand, operator, rightOperand))
    currentEquation['leftOperand'] = result;
    currentEquation['operator'] = '';
    currentEquation['rightOperand'] = '';
    updateDisplay(currentEquation['leftOperand']);
    removePushedStates();
}


function removePushedStates() {
    const operatorNodes = document.querySelectorAll('.calc-operator-button');
    operatorNodes.forEach(button => {
        buttonClasses = Array.from(button.classList);
        if (buttonClasses.includes('calc-operator-button-active')) {
            button.classList.remove('calc-operator-button-active');
        }
    });
}


function subtract(x, y) {
    return x - y;
}


function undoDisplay() {
    const display = document.querySelector('.calc-display');
    if (isDisplayEmpty()) {
        return;
    }

    currentSideOfOperator = getCurrentSideOfOperator();
    currentOperand = currentEquation[currentSideOfOperator];
    newOperand = currentOperand.slice(0, -1);
    currentEquation[currentSideOfOperator] = newOperand;
    updateDisplay(currentEquation[currentSideOfOperator]);
}


function updateDisplay(newContents) {
    const display = document.querySelector('.calc-display');
    display.textContent = newContents.slice(0, MAX_DISPLAY_LENGTH);
}


const MAX_DISPLAY_LENGTH = 9;
const currentEquation = {
    'leftOperand': '',
    'operator': '',
    'rightOperand': '',
}
createInterface();
